# Proxy Reverse / Certificates SSL


1. [Containers](#1-containers)
2. [Infrastructure](#2-infrastructure)
3. [Requisites](#3-requisites)
4. [Installation](#4-installation)
5. [Pre-execution](#5-pre-execution)
6. [Execution](#6-execution)
7. [SSL (Optional)](#7-ssl-optional)

## 1. Containers

- *Reverse Proxy* - Nginx;
- *Letsencrypt* - Nginx;

2 Containers.

## 2. Infrastructure

![Infrastructure Proxy](infra-proxy.png)

## 3. Requisites

- Git version 2 or above
- Docker version 17.09.1-ce or above (https://docs.docker.com/install/)
- Docker Compose 1.20.1 or above (https://docs.docker.com/compose/install)

## 4. Installation

Clone the repository with:

<pre>
user@host:~$ <b>git clone https://gitlab.com/integrativebioinformatics/reverse_proxy_apps.git</b>
</pre>


## 5. Pre-Execution

First, it is important that the project environment is running with docker-compose, it should not export ports 80 or 443, the proxy will reach the front-end/webserver/container through the docker's own network. So let's take the steps to make this work.

### 5.1. Edit docker-compose configuration file - External project

It is important that the proxy finds the webserver/container that is running on port 80 (no export port), so we will define a network in the corresponding service for this purpose, besides that we must also define a friendly name, so the `container_name` parameter must obey the `app_webserver` pattern. Where `app` is the same as the host name in the FQDN definition, example `app`.domain.com.

Below is a small example of a docker-compose config file in external project:

<pre>
version: "3.4"

services:

    webserver_mdp:
        build: ./build/webserver
        container_name: <b>app_webserver</b>
        restart: always
        volumes:
            - ./volumes/frontend:/var/www/html
        depends_on:
             - php_backend
        networks:
            <b>- webserver</b>
            - backend

    backend_app:
        build: ./build/backend
        container_name: app_backend
        restart: always
        volumes:
            - ./volumes/backend:/var/www/html
        networks:
             - backend

networks:
    <b>webserver:</b>
    php:
</pre>

Let's assume that the external project directory name is `appname`, this information is important because docker-compose creates the **general network name** (view with `docker network ls`) using the DIRECTORY_NETWORK standard. Since we want to connect the network from the external project webserver to the proxy network, then in our example the network of interest would have the general name `appname_webserver`.

### 5.2. Edit docker-compose configuration file - Reverse Proxy project

<pre>
version: "3.4"

services:

    proxy:
        build: ./proxy
        container_name: proxy
        ports:
            - "80:80"
            - "443:443"
        depends_on:
            - letsencrypt
        volumes:
            - certs:/etc/letsencrypt
        environment:
            - APPS_NUMBER=${APPS_NUMBER}
            <b>- APPNAME1=${APPNAME1}
            - DOMAIN1=${DOMAIN1}</b>
        networks:
            - proxy_lets
            <b>- appname_webserver</b>
        command: /bin/bash -c "genAppNginx -n ${APPS_NUMBER} -o /etc/nginx/conf.d/ && nginx -g 'daemon off;'"

    letsencrypt:
        build: ./letsencrypt
        container_name: letsencrypt
        volumes:
            - certs:/etc/letsencrypt
        networks:
            - proxy_lets

volumes:
    certs:

networks:
    proxy_lets:
    <b>appname_webserver:
        external: true</b>
</pre>

For multiple projects, simply add the information about the respective project, in a similar way to the highlighted areas in files.

### 5.3. Edit .env file - Reverse Proxy project

Edit file `.env` in root directory on repository informing enviremont variables, example content:

<pre>
user@host:~/reverse_proxy_apps$ cat .env
EMAIL_LETS=brunogomescorreia@gmail.com
APPS_NUMBER=<b>1</b>
APPNAME1=<b>app</b>
DOMAIN1=<b>domain.com</b>
</pre>

For multiple projects, simply add the information about the respective project, in a similar way to the highlighted areas in files.

## 6. Execution

In the root repository, execute the next command (**is very important the external project running before**):

<pre>
user@host:~/reverse_proxy_apps$ <b>docker-compose up --build -d</b>
</pre>

The option `-d` execute containers in background.

**OBS.:** The same command can be executed after added parameters for a new app.

## 7. SSL (Optional)

Per default, the first execution use no-ssl. Case need use ssl, after execution (with containers running), do following:

<pre>
user@host:~/reverse_proxy_apps$ <b>docker exec letsencrypt  bash -c 'certbot certonly --email ${EMAIL_LETS} -a webroot --webroot-path=/usr/share/nginx/html -d app.domain.com --agree-tos'</b>
</pre>

**OBS1.:** Use option -d according with need. 

**OBS2.:** The url `app.domain.com` is FQDN to SSL certificate.

After, in projecty directory:

<pre>
user@host:~/reverse_proxy_apps$ <b>docker-compose restart proxy</b>
Restarting proxy ... done
</pre>

Enjoy!
